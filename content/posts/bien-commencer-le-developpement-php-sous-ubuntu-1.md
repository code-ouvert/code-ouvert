---
title: "Bien commencer le développement PHP sous Ubuntu Eps1 : installation de APACHE, MySQL, PHP"
date: 2021-10-17T10:00:00+01:00
author: "Yassin El Kamal NGUESSU"
authorTwitter: "" # do not include @
cover: "" # /img/projet01/01.png
tags: ["php", "ubuntu", "MySQL", "apache2", "lamp", "linux", "le developpeur web"]
keywords: ["code ouvert", "php", "ubuntu", "MySQL", "apache2", "lamp", "linux", "Bien commencer le développement PHP sous Ubuntu", "le developpeur web"]
description: "C’est parti, vous voulez vous lancer dans Ubuntu pour faire du développement PHP, mais vous ne savez pas par où commencer? Qu’est ce que nous allons voir aujourd’hui ? Installation d’apache, de MySQL, de PHP et configuration du dossier www, bien connu de ceux qui viennent de Windows."
showFullContent: false
draft: false
---
{{< youtube AN--RepBBj4 >}}

## Objectifs

C'est parti, vous voulez vous lancer dans **Ubuntu** pour faire du **développement PHP**, mais vous ne savez pas par où commencer?

Qu'est ce que nous allons voir aujourd'hui ? **Installation d'apache, de MySQL, de PHP et configuration du dossier www**, bien connu de ceux qui viennent de Windows.

## Pré-requis : 

- Une installation fraîche (ou à peu près fraîche) d'Ubuntu.

## Ressources :

- https://doc.ubuntu-fr.org/lamp
