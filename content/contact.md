+++
title = "Contact"
+++

## Nos coordoonées

>Téléphone : +237 6 81 07 60 39
>
>Whatsapp :  +237 6 81 07 60 39
>
>Adresse e-mail: leyassino@gmail.com 

## Vous avez besoin …

### D’un developpeur pour un projet d’application Web ?

Vous êtes à la recherche d’un developpeur web Freelance pour concretiser une idée, un projet d’application ? Je peut vous accompagner sur un projet de A à Z, de la conception à la mise en production. Je travail avec le framework Symfony (PHP). [Faire une demande de developpement](#)

### D’un accompagnement sur les pipelines CI/CD ?

Vous cherchez à faciliter et fiabiliser votre processus de developpement ? Je peut vous aider à construire vos pipelines d’intégration continue et vos pipelines de déploiement continu. [Faire une demande d’accompagnement CI/CD](#)

### D’une formation sur-mesure ?

Vous êtes à la recherche d’une formation pour vos équipes de devéloppeurs ? Je peut produire pour vous du contenu pédagogique sur-mesure (Textes, Tuto et vidéos) sur de nombreuse thématiques : Docker, CI/CD, Environnement de développement, Symfony, Gestion de projets… [Faire une demande de formation](#)

### Partenariat pour la chaine YouTube ?

Vous êtes à la recherche de visibilité sur YouTube (et Twitter) auprès d’une audience de développeurs francophone ? N’hésitez pas à me contacter ! [Proposer un partenariat](#)

